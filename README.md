# Kylobytes Website

Website for Kylobytes (currently at <https://www.kylobytes.com>).

This website uses [Zola](https://www.getzola.org), a static site generator.

## License

### Source Code

The source code for this website is licensed under the [GNU General Public 
License verion 3.0 or later](https://www.gnu.org/licenses/gpl-3.0.txt)

![GNU General Public License version 3.0 or later](assets/gplv3-or-later.svg)

### Content

The content and media on the website is licensed under the 
[Creative Commons Attribution-ShareAlike 4.0
International](https://creativecommons.org/licenses/by-sa/4.0/)
License

![Creative Commons Attribution-ShareAlike 4.0 International](assets/by-sa.svg)
